<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/avail_locations', 'ParkingLocController@avail_loc')->name('locations.availible');
Route::post('/addloc', 'ParkingLocController@Addlocations')->name('locations.add');
Route::post('/parkVehicle', 'ParkingController@carPark')->name('vehicle.park');
Route::get('/exitVehicle/{id}', 'ParkingController@exitCar')->name('vehicle.exit');
Route::get('/parkedVehicles', 'ParkingController@parkedCars')->name('vehicle.parked.all');
Route::get('/report', 'ParkingController@records')->name('vehicle.records');
Route::get('/dashboard', 'ParkingController@dashboard')->name('vehicle.records');
