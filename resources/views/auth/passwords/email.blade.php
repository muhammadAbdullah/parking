@extends('layouts.app')

@section('content')
    <body class="register-page">
    <!-- Navbar -->

    @include('layouts.topnavauth')
    <!-- End Navbar -->
<div class="wrapper wrapper-full-page ">
    <div class="full-page register-page section-image" filter-color="black" data-image="../assets/img/bg/cover.png">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="content" style="padding-bottom: 21vh !important">
            <div class="container">
                <div class="col-lg-4 col-md-6 ml-auto mr-auto">
                    <form  action="{{ route('password.email') }}" method="POST">
                        @csrf
                        <div class="card card-login">
                            <div class="card-header text-center" style="padding: 3.0rem !important;">
                                <img src="../assets/img/Logo.png">
                                <br>
                            </div>
                            <div class="card-body ">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="input-group">
                                    <div class="input-group-prepend">
                      <span class="input-group-text ">
                        <i class="nc-icon nc-single-02"></i>
                      </span>
                                    </div>
                                    <input id="email" type="email" style="height: 100%" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="Email">
                                    @error('email')
                                    <span class="invalid-feedback text-center" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <br/>

                            </div>
                            <div class="card-footer" style="text-align: center!important;">
                                <input type="submit" value="Reset Password" class="btn btn-danger btn-round btn-block mb-3 bontti_button"  style="background-color: #c21d30!important;">

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <footer class="footer footer-black  footer-white ">
            {{--            <div class="container-fluid">--}}
            {{--                <div class="row">--}}
            {{--                    <nav class="footer-nav">--}}
            {{--                        <ul>--}}
            {{--                            <li>--}}
            {{--                                <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>--}}
            {{--                            </li>--}}
            {{--                            <li>--}}
            {{--                                <a href="http://blog.creative-tim.com/" target="_blank">Blog</a>--}}
            {{--                            </li>--}}
            {{--                            <li>--}}
            {{--                                <a href="https://www.creative-tim.com/license" target="_blank">Licenses</a>--}}
            {{--                            </li>--}}
            {{--                        </ul>--}}
            {{--                    </nav>--}}
            {{--                    <div class="credits ml-auto">--}}
            {{--              <span class="copyright">--}}
            {{--                ©--}}
            {{--                <script>--}}
            {{--                  document.write(new Date().getFullYear())--}}
            {{--                </script>, made with <i class="fa fa-heart heart"></i> by Creative Tim--}}
            {{--              </span>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        </footer>
    </div>
</div>
</body>
@endsection
