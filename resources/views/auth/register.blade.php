@extends('layouts.app')

@section('content')
    <body class="register-page">
    <!-- Navbar -->
    @include('layouts.topnavauth')
    <!-- End Navbar -->
    <div class="wrapper wrapper-full-page ">
        <div class="full-page register-page section-image" filter-color="black" data-image="assets/img/bg/cover.png">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content" style="padding-bottom: 21vh !important">
                <div class="container">
                    <div class="col-lg-4 col-md-6 ml-auto mr-auto">
                            <div class="card card-signup text-center">
                                <div class="card-header ">
                                    <h4 class="card-title">Register</h4>

                                </div>
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                <div class="card-body ">

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="nc-icon nc-single-02"></i>
                        </span>
                                            </div>
                                            <input id="name" style="height: 100%" type="text" placeholder="{{ __('Name') }}" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus>

                                            @error('name')
                                            <span class="invalid-feedback text-center" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="nc-icon nc-circle-10"></i>
                        </span>
                                            </div>
                                            <input id="email" style="height: 100%" type="email" placeholder="{{ __('E-Mail Address') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback text-center" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="nc-icon nc-email-85"></i>
                        </span>
                                            </div>
                                            <input id="password" style="height: 100%" type="password" placeholder="{{ __('Password') }}" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback text-center" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="nc-icon nc-email-85"></i>
                        </span>
                                            </div>
                                            <input id="password-confirm" style="height: 100%" type="password" placeholder="{{ __('Confirm Password') }}" class="form-control" name="password_confirmation" autocomplete="new-password">

                                        </div>


                                </div>
                                <div class="card-footer ">
                                    <button type="submit" class="btn btn-danger btn-round" style="background-color: #c21d30!important;">
                                        {{ __('Register') }}
                                    </button>
                                    <br>
                                    <br>
                                    <span style="margin-top: 1rem!important;" >Already have an account!</span>
                                    <br>
                                    <a href="{{ route('login') }}"> <input type="button" style="margin-top: 0.5rem;background-color: #c21d30!important;" value="Sign In Here" class="btn btn-danger btn-round btn-block mb-3 ">
                                    </a>
                                </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>
            <footer class="footer footer-black  footer-white ">
{{--                <div class="container-fluid">--}}
{{--                    <div class="row">--}}
{{--                        <nav class="footer-nav">--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="http://blog.creative-tim.com/" target="_blank">Blog</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="https://www.creative-tim.com/license" target="_blank">Licenses</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </nav>--}}
{{--                        <div class="credits ml-auto">--}}
{{--              <span class="copyright">--}}
{{--                ©--}}
{{--                <script>--}}
{{--                  document.write(new Date().getFullYear())--}}
{{--                </script>, made with <i class="fa fa-heart heart"></i> by Creative Tim--}}
{{--              </span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </footer>
        </div>
    </div>

    </body>
@endsection
