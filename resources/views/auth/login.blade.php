@extends('layouts.app')

@section('content')
    <body class="register-page">
    <!-- Navbar -->

    @include('layouts.topnavauth')
    <!-- End Navbar -->
    <div class="wrapper wrapper-full-page ">
        <div class="full-page register-page section-image" filter-color="black" data-image="assets/img/bg/cover.png">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content" style="padding-bottom: 21vh !important">
                <div class="container">
                    <div class="col-lg-4 col-md-6 ml-auto mr-auto">
                        <form  action="{{ route('login') }}" method="POST">
                            @csrf
                            <div class="card card-login">
                                <div class="card-header text-center " style="padding: 2.0rem !important;">
{{--                                    <img src="assets/img/Logo.png">--}}
                                    <h2>Parking</h2>
                                    <br>
                                </div>
                                <div class="card-body ">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="nc-icon nc-single-02"></i>
                      </span>
                                        </div>
                                        <input id="email" type="email" placeholder="Email" style="height: 100%" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" autofocus>


                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="nc-icon nc-key-25"></i>
                      </span>
                                        </div>
                                        <input id="password" type="password" placeholder="Password" style="height: 100%" class="form-control @error('password') is-invalid @enderror @error('email') is-invalid @enderror" name="password"  autocomplete="current-password">

                                        @error('email')
                                        <span class="invalid-feedback text-center" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                        @error('password')
                                        <span class="invalid-feedback text-center" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <br/>

                                </div>
                                <div class="card-footer" style="text-align: center!important;">
                                    <input type="submit" value="Login" class="btn btn-danger btn-round btn-block mb-3 bontti_button"  style="background-color: #c21d30!important;">
                                    <a class="bonnti-color-text" href="password/reset"><span>Forgot Password?</span></a>
                                    <br>
                                    <br>
                                    <span  >Don't have an account?</span>
                                    <br>
                                    <a href="{{ route('register') }}"> <input type="button" style="margin-top: 0.5rem;background-color: #c21d30!important;" value="Sign up Here" class="btn btn-danger btn-round btn-block mb-3 bontti_button">
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <footer class="footer footer-black  footer-white ">
{{--                <div class="container-fluid">--}}
{{--                    <div class="row">--}}
{{--                        <nav class="footer-nav">--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="http://blog.creative-tim.com/" target="_blank">Blog</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="https://www.creative-tim.com/license" target="_blank">Licenses</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </nav>--}}
{{--                        <div class="credits ml-auto">--}}
{{--              <span class="copyright">--}}
{{--                ©--}}
{{--                <script>--}}
{{--                  document.write(new Date().getFullYear())--}}
{{--                </script>, made with <i class="fa fa-heart heart"></i> by Creative Tim--}}
{{--              </span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </footer>
        </div>
    </div>
    </body>
@endsection
