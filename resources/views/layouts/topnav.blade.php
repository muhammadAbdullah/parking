@guest

@else
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent" style="float: left">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-minimize">
                <button id="minimizeSidebar" class="btn btn-icon btn-round">
                    <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
                    <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
                </button>
            </div>
            <div class="navbar-toggle" >
                <button type="button" class="navbar-toggler">
                    <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
                    <i class="nc-icon nc-bullet-list-67 text-center visible-on-sidebar-regular"></i>
                </button>
            </div>

        </div>
    </div>
</nav>
{{--<nav class="navbar  fixed-top  " style="margin-left: 4%;max-width: 50% ;margin-right: 550px!important">--}}

{{--        <div class=" navbar-collapse justify-content-end text-right" id="navigation" style="">--}}
{{--            <ul class="navbar-nav">--}}
{{--                @guest--}}

{{--                @else--}}
{{--                    <li class="nav-item dropdown">--}}
{{--                        <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                         <span style="color: black">--}}
{{--                             {{ Auth::user()->name }}--}}
{{--                             </span>--}}

{{--                        </a>--}}
{{--                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">--}}
{{--                            <router-link to="/profile" class="dropdown-item" > profile</router-link>--}}
{{--                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();--}}
{{--                            document.getElementById('logout-form').submit();">--}}
{{--                                {{ __('Logout') }}--}}
{{--                            </a>--}}
{{--                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
{{--                                @csrf--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                @endguest--}}

{{--            </ul>--}}
{{--        </div>--}}
{{--</nav>--}}
@endguest
