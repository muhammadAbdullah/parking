
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.head')
</head>

@guest

@else
    <body class="">
    <div class="wrapper" id="app" >
        @include('layouts.sidebar')
        <div class="main-panel"  >
            <!-- Navbar -->
            @include('layouts.topnav')
            <br>
            <div class="content">

                @endguest


                @yield('content')

                @guest

                @else
            </div>

        </div>
    </div>
    @endguest

<!--   Core JS Files   -->
        <script src="{{ asset('assets/js/core/jquery.min.js') }}" ></script>
        <script>
            window.onload = function() {
                jQuery('input[type=text]').blur();
            };
        </script>
        <script src="{{ asset('assets/js/core/popper.min.js') }}" ></script>
        <script src="{{ asset('assets/js/core/bootstrap.min.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/moment.min.js') }}" ></script>
{{--        <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>--}}
        <script src="{{ asset('js/app.js?v=37') }}" defer></script>
        <script src="{{ asset('assets/js/plugins/bootstrap-switch.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/sweetalert2.min.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/jquery.validate.min.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/jquery.bootstrap-wizard.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/bootstrap-selectpicker.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/bootstrap-datetimepicker.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/jquery.dataTables.min.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/bootstrap-tagsinput.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/jasny-bootstrap.min.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/fullcalendar.min.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/jquery-jvectormap.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/nouislider.min.js') }}" ></script>
        <script src="{{ asset('assets/js/paper-dashboard.min.js?v=2.0.1') }}" ></script>
        <script src="{{ asset('assets/js/plugins/bootstrap-notify.js') }}" ></script>
        <script src="{{ asset('assets/js/plugins/chartjs.min.js') }}" ></script>
        <script src="{{ asset('assets/demo/demo.js') }}" ></script>

       <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDF8X_WE2y-BDWOxj_yM6ZYfgIYAA-evdU&libraries=places&language=en"></script>
        <script src="{{ asset('assets/js/jquery.geocomplete.min.js?v=1.0.1') }}" ></script>

        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
        <script type="text/javascript"  src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

        <script>
            $(document).ready(function() {
                demo.checkFullPageBackgroundImage();
            });
        </script>

</body>

</html>
