@guest

@else
<div class="sidebar" data-color="red" data-active-color="danger" >
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo" >
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="assets/img/logo-small.png">
            </div>
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
           Parking Lot
            <!-- <div class="logo-image-big">
              <img src="assets/img/logo-big.png">
            </div> -->
        </a>
    </div>
    <div >
        <div class="sidebar-wrapper">
            <div class="user">
{{--                <div class="photo">--}}
{{--                    <img src="../assets/img/faces/ayo-ogunseinde-2.jpg" />--}}
{{--                </div>--}}
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" class="collapsed">
              <span class="text-center">
                {{ Auth::user()->name }}
                <b class="caret"></b>
              </span>
                    </a>
                    <div class="clearfix"></div>
                    <div class="collapse" id="collapseExample">
                        <ul class="nav text-center navbar-toggle">
{{--                            <li>--}}
{{--                                <router-link to="/profile" class="text-center" >--}}
{{--                                    <i class="nc-icon nc-album-2" style="float: right"></i>--}}
{{--                                    <p >Profile</p>--}}
{{--                                </router-link>--}}
{{--                            </li>--}}

                            <li>
                                <a class="" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
{{--                                    <i class="nc-icon nc-button-power"></i>--}}
                                    <p class="sidebar-normal">{{ __('Logout') }}</p>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav sidebarbuttons navbar-toggle ">
                <li class="navbar-toggler">
                    <router-link to="/" >
                        <i class="nc-icon nc-bank"></i>
                        <p >Home</p>
                    </router-link>
                </li>
                <li class="navbar-toggler">
                    <router-link to="/addVehicle" >
                        <i class="nc-icon nc-money-coins"></i>
                        <p>vehicle Entrance</p>
                    </router-link>
                </li>
                <li class="navbar-toggler">
                    <router-link to="/exitVehicle" >
                        <i class="nc-icon nc-delivery-fast"></i>
                        <p>vehicle Exit</p>
                    </router-link>
                </li>
                <li class="navbar-toggler">
                    <router-link to="/report" >
                        <i class="nc-icon nc-book-bookmark"></i>
                        <p>Records</p>
                    </router-link>
                </li>
{{--                <li class="navbar-toggler">--}}
{{--                    <router-link to="/save" >--}}
{{--                        <i class="fa fa-plug"></i>--}}
{{--                        <p>Tips to Save</p>--}}
{{--                    </router-link>--}}
{{--                </li>--}}
{{--                <li class="navbar-toggler">--}}
{{--                    <a href = "mailto: info@bonnti.com"  >--}}
{{--                        <i class="nc-icon nc-chat-33"></i>--}}
{{--                        <p>Contact us</p>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="navbar-toggler">--}}
{{--                        <a  href="{{ route('logout') }}" onclick="event.preventDefault();--}}
{{--                            document.getElementById('logout').submit();">--}}
{{--                            <i class="nc-icon nc-button-power"></i><p> {{ __('Logout') }}</p>--}}
{{--                        </a>--}}
{{--                        <form id="logout" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
{{--                            @csrf--}}
{{--                        </form>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a data-toggle="collapse" href="#componentsExamples">--}}
{{--                        <i class="nc-icon nc-layout-11"></i>--}}
{{--                        <p>--}}
{{--                            Type--}}
{{--                            <b class="caret"></b>--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                    <div class="collapse " id="componentsExamples">--}}
{{--                        <ul class="nav">--}}
{{--                            <li>--}}
{{--                                <router-link to="/type" >--}}
{{--                                    <i class="nc-icon nc-bank"></i>--}}
{{--                                    <p>View Type</p>--}}
{{--                                </router-link>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
            </ul>
        </div>
    </div>

</div>
@endguest
