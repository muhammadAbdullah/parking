<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carImage extends Model
{
    protected $fillable = [
        'name', 'type',
    ];
}
