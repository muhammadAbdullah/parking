<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class parking extends Model
{
    protected $fillable = [
        'carId', 'locId','exited_at'
    ];
    public function car()
    {
        return $this->hasOne('App\car', 'id', 'carId');
    }
    public function location()
    {
        return $this->hasOne('App\parkingLoc', 'id', 'locId');
    }
}
