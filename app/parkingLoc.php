<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class parkingLoc extends Model
{
    protected $fillable = [
        'locationID', 'floor','status'
    ];
}
