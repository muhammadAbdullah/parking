<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class car extends Model
{
    protected $fillable = [
        'CarName', 'CarNumber', 'CNIC', 'imageId'
    ];
    public function image()
    {
        return $this->hasOne('App\CarImage', 'id', 'imageId');
    }
}

