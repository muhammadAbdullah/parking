<?php

namespace App\Http\Controllers;

use App\parkingLoc;
use Illuminate\Http\Request;

class ParkingLocController extends Controller
{
    public function Addlocations(Request $request)
    {
        try {
           $data = parkingLoc::where('floor','=',$request['floor'])->select('locationID')->latest()->first();
           $locID =$data['locationID'];
            for($i = 0;$i<$request['number'];$i++)
            {
                $locID += 1;
                $result=parkingLoc::create([
                    'locationID' => $locID,
                    'floor' =>$request['floor'],
                    'status' => true
                ]);
            }

        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
                'error' => 'yes',
                'car' => null
            ]);
        }
        return response()->json([
            'message' => 'Locations added',
            'error' => 'yes',
            'car' => null
        ]);

    }
    public function avail_loc(){
        $result = parkingLoc::where('status','=',True)->select('id','locationID','floor')->get()->all();
        return response()->json([
            'message' => 'Availible locations',
            'error' => 'yes',
            'locations' => $result
        ]);
    }


}
