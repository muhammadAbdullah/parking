<?php

namespace App\Http\Controllers;

use App\car;
use App\carImage;
use App\parking;
use App\parkingLoc;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ParkingController extends Controller
{
    public function carPark(Request $request){
        try {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $imageName);
            $image = carImage::create(
                [
                    'name' => $imageName,
                    'type' => $request->image->getClientOriginalExtension(),
                ]
            );
            $car = car::create(
                [
                    'CarName' => $request['name'],
                    'CarNumber' => $request['number'],
                    'CNIC' => $request['cnic'],
                    'imageId' => $image->id,
                ]
            );

            $parking = parking::create([
                'carId' => $car->id,
                'locId' => $request['locationID'],
            ]);

        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
                'error' => 'yes',
                'car' => null
            ]);
        }
        return response()->json([
            'message' => 'Car added',
            'error' => 'no',
            'car' => $car,
        ]);
    }
    public function exitCar($id){
        try{
            $result = parking::where('id','=',$id )->update([
                'exited_at' => Carbon::now()
            ]);
        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
                'error' => 'yes',
                'result' => false
            ]);
        }
        return response()->json([
            'message' => 'Car added',
            'error' => 'no',
            'result' => $result,
        ]);

    }
    public function parkedCars(){
        $result = parking::whereNull('exited_at')->with('car')->with('location')->get()->all();
        $carsCount=parking::whereNull('exited_at')->count();
        $moneyCollected  = parking::whereDate('exited_at', Carbon::today())->count();
        $moneyCollected= $moneyCollected *20;
        return response()->json([
            'message' => 'Parked cars',
            'error' => 'no',
            'result' => $result,
            'moneyCollected' => $moneyCollected,
            'carParked' => $carsCount,
        ]);
    }
    public function records(){
        $result = parking::with('car')->with('location')->get()->all();
        $parked = parking::whereNotNull('exited_at')->with('car')->with('location')->get()->count();
        $exited = parking::whereNull('exited_at')->with('car')->with('location')->get()->count();
        $moneyCollected  = parking::whereNotNull('exited_at')->count();
        $totalcars  = parking::get()->count();
        $moneyCollected= $moneyCollected *20;
        $chart_data =array($parked,$exited);

        return response()->json([
            'message' => 'Parked cars',
            'error' => 'no',
            'result' => $result,
            'chart_Data'=> $chart_data,
            'total_money' =>$moneyCollected,
            'total_cars' => $totalcars
        ]);
    }
    public function dashboard(){
        try {
            $totalCarParked = parking::whereNotNull('exited_at')->count();
            $moneyCollectedtotal =$totalCarParked*20;
            $moneyCollected  = parking::whereDate('exited_at', Carbon::today())->count();
            $moneyCollected= $moneyCollected *20;

            $result = parkingLoc::where('status','=',True)->get()->count();

        }catch (\Exception $e)
        {
            return response()->json([
                'message' => $e->getMessage(),
                'error' => 'yes',
                'result' => false
            ]);        }
        return response()->json([
            'message' => 'Parked cars',
            'error' => 'no',
            'total_money' =>$moneyCollectedtotal,
            'total_cars' => $totalCarParked,
            'money_Today' => $moneyCollected,
            'avail_loc' => $result
        ]);
    }
}
